/*
 * Copyright © 2013 - 2015 Atlassian Pty Ltd. Licensed under the Apache License, Version
 *  2.0 (the "License"); you may not use this file except in compliance with the License. You
 *  may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless
 *  required by applicable law or agreed to in writing, software distributed under the License is
 *  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions
 *  and limitations under the License.
 */

package com.atlassian.jira.plugins.projectclone.copiers;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.context.manager.JiraContextTreeManager;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigManager;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.persistence.FieldConfigSchemePersisterImpl;
import com.atlassian.jira.plugins.projectclone.extensions.ProjectSettingsCopier;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.collections.MultiMap;
import org.ofbiz.core.entity.GenericValue;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class CustomFieldsCopier implements ProjectSettingsCopier {
    public static final Function<Project, Long> PROJECT_TO_ID = new Function<Project, Long>() {
        @Override
        public Long apply(@Nullable Project input) {
            return input.getId();
        }
    };

	@Autowired
	protected CustomFieldManager customFieldManager;
	@Autowired
	protected FieldConfigSchemeManager fieldConfigSchemeManager;
	@Autowired
	protected ConstantsManager constantsManager;
	@Autowired
	protected FieldConfigManager fieldConfigManager;

	@Override
	public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
		final List<CustomField> customFields = customFieldManager
				.getCustomFieldObjects(project.getId(), ConstantsManager.ALL_ISSUE_TYPES);

		final Function<GenericValue, String> name = new Function<GenericValue, String>() {
			@Override
			public String apply(@Nullable GenericValue input) {
				return input.getString("name");
			}
		};

		final Function<GenericValue, Long> gv2id = new Function<GenericValue, Long>() {
			@Override
			public Long apply(@Nullable GenericValue input) {
				return input.getLong("id");
			}
		};

		for (CustomField customField : customFields) {
			if (!customField.isGlobal() && customField.isEnabled() && !customField.isAllProjects()) {
				final List<FieldConfigScheme> schemes = fieldConfigSchemeManager.getConfigSchemesForField(customField);
				for(FieldConfigScheme scheme : schemes) {
					FieldConfigScheme configScheme = new FieldConfigScheme.Builder(scheme).toFieldConfigScheme();

					// need to extend it if it's scope is limited to the project
					final List<Project> projects = Lists.newArrayList(customField.getAssociatedProjectObjects());
					if (!projects.contains(project)) {
						continue; // this scheme doesn't include the original project skip it!
					}

					projects.add(newProject);

					final Long[] projectCategories = Iterables
							.<Long>toArray(Iterables.transform(customField.getAssociatedProjectCategories(), gv2id),
									Long.class);

					final Long[] projectIds = Iterables.<Long>toArray(Iterables.transform(projects, PROJECT_TO_ID), Long.class);

					final List<JiraContextNode> contexts = CustomFieldUtils
							.buildJiraIssueContexts(false, projectCategories, projectIds, ComponentManager
									.getComponentInstanceOfType(JiraContextTreeManager.class));

					final List<GenericValue> issueTypes = customField.getAssociatedIssueTypes();

					// Update so keep the old config
					if (issueTypes != null)
					{
						// Since we know that there is only one config
						final Long configId = getFieldConfigIds(scheme)[0];
						final FieldConfig config = fieldConfigManager.getFieldConfig(configId);
						final Map<String, FieldConfig> configs = new HashMap<String, FieldConfig>(issueTypes.size());
						for (final GenericValue issueType : issueTypes)
						{
							final String issueTypeId = issueType == null ? null : issueType.getString(
									FieldConfigSchemePersisterImpl.ENTITY_ID);
							configs.put(issueTypeId, config);
						}
						configScheme = new FieldConfigScheme.Builder(configScheme).setConfigs(configs).toFieldConfigScheme();
					}

					// real update
					fieldConfigSchemeManager.updateFieldConfigScheme(configScheme, contexts, customField);
					customFieldManager.refreshConfigurationSchemes(customField.getIdAsLong());
				}
			}
		}
	}

	protected Long[] getFieldConfigIds(FieldConfigScheme configScheme) {
		// Set the config
		final MultiMap configMap = configScheme.getConfigsByConfig();
		if (configMap == null)
		{
			return new Long[0];
		}
		else
		{
			final Set entries = configScheme.getConfigsByConfig().keySet();
			final Long[] fieldConfigIds = new Long[entries.size()];
			int i = 0;
			for (final Object entry : entries)
			{
				final FieldConfig config = (FieldConfig) entry;
				fieldConfigIds[i] = config.getId();
				i++;
			}
			return fieldConfigIds;
		}
	}
}
